-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: May 14, 2020 at 03:06 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `szonlineedu`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `studentid` int(11) NOT NULL,
  `quizid` int(11) NOT NULL,
  `resultPerc` float DEFAULT NULL,
  `answerArray` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `passwordresets`
--

CREATE TABLE `passwordresets` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `creationDateTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `secret` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `passwordresets`
--

INSERT INTO `passwordresets` (`id`, `userId`, `creationDateTime`, `secret`) VALUES
(1, 6, '2020-05-12 07:21:31', 'uFSBIuijyHDocyYPqmmTfnAsP2OLaEkVDWvL2RwiFzyL0FFVt9botz9JUKDo');

-- --------------------------------------------------------

--
-- Table structure for table `questions_en`
--

CREATE TABLE `questions_en` (
  `id` int(11) NOT NULL,
  `question_description` text NOT NULL,
  `a1` text NOT NULL,
  `a2` text NOT NULL,
  `a3` text NOT NULL,
  `a4` text NOT NULL,
  `rightAnswer` varchar(100) NOT NULL,
  `quizid` int(11) NOT NULL,
  `photo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions_en`
--

INSERT INTO `questions_en` (`id`, `question_description`, `a1`, `a2`, `a3`, `a4`, `rightAnswer`, `quizid`, `photo`) VALUES
(1, 'Which country\'s flag?', 'USA', 'Canada', 'Japan', 'Brazil', 'Canada', 4, 'uploadimages/canada.png'),
(2, 'Which country\'s flag?', 'USA', 'Canada', 'Japan', 'Brazil', 'Brazil', 4, 'uploadimages/brazil.png'),
(3, 'Which country\'s flag?', 'USA', 'Canada', 'Japan', 'Brazil', 'USA', 4, 'uploadimages/usa.png'),
(4, 'Which country\'s flag?', 'USA', 'Canada', 'Japan', 'Brazil', 'Japan', 5, 'uploadimages/japan.png'),
(5, 'Which country\'s flag?', 'USA', 'Canada', 'Japan', 'France', 'France', 5, 'uploadimages/france.png');

-- --------------------------------------------------------

--
-- Table structure for table `quizs`
--

CREATE TABLE `quizs` (
  `id` int(11) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `questionIncluded` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quizs`
--

INSERT INTO `quizs` (`id`, `description`, `questionIncluded`) VALUES
(1, 'Quiz1', 'Which country is largest in the world?'),
(2, 'Quiz2', 'Which river is the longest in the world?'),
(3, 'Quiz3', 'Which mount is the highest in the world?'),
(4, 'sdvsdvsdvsvdsdvds', '1;2;3'),
(5, 'fdsggregerg', '1;2;3');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `parentid` int(11) NOT NULL,
  `bonus_en` int(11) NOT NULL,
  `bonus_sc` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `studentcode` varchar(12) NOT NULL,
  `groupid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(12) NOT NULL,
  `address` varchar(50) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `address`, `phone`, `isAdmin`) VALUES
(2, 'JohnAbbott', 'saperpsh@hotmail.com', '1q2w3e', NULL, NULL, NULL),
(3, 'bbbbbb', 'saperpsh1@hotmail.co', '1q2w3e', NULL, NULL, NULL),
(4, 'Francois', 'Francois@gmail.com', '1q2w3e4r', NULL, NULL, 1),
(6, 'MingZi', 'm5145538386@gmail.com', '1q2w3e4r', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_answer` (`studentid`),
  ADD KEY `fk_quiz_answer` (`quizid`) USING BTREE;

--
-- Indexes for table `passwordresets`
--
ALTER TABLE `passwordresets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userId` (`userId`);

--
-- Indexes for table `questions_en`
--
ALTER TABLE `questions_en`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quizid` (`quizid`);

--
-- Indexes for table `quizs`
--
ALTER TABLE `quizs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `photo` (`photo`),
  ADD UNIQUE KEY `studentcode` (`studentcode`),
  ADD UNIQUE KEY `groupid` (`groupid`),
  ADD KEY `fk_parent` (`parentid`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `passwordresets`
--
ALTER TABLE `passwordresets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `questions_en`
--
ALTER TABLE `questions_en`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `quizs`
--
ALTER TABLE `quizs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `fk_quiz_answer` FOREIGN KEY (`quizid`) REFERENCES `quizs` (`id`),
  ADD CONSTRAINT `fk_student_answer` FOREIGN KEY (`studentid`) REFERENCES `students` (`id`);

--
-- Constraints for table `questions_en`
--
ALTER TABLE `questions_en`
  ADD CONSTRAINT `questions_en_ibfk_1` FOREIGN KEY (`quizid`) REFERENCES `quizs` (`id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `fk_parent` FOREIGN KEY (`parentid`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
