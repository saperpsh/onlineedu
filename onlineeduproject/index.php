<?php

require_once "setup.php";

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\UploadedFileInterface;
use Slim\Views\Twig;

require_once "account.php";

$app->get('/forbidden', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_internal', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/error_notfound', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_notfound.html.twig');
});

$app->get('/session', function (Request $request, Response $response, array $args) {
    $body = "<pre>\n\$_SESSION:\n" . print_r($_SESSION, true);
    $response->getBody()->write($body);
    return $response;
});


$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'index.html.twig');
});



$app->get('/account/{id}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }
    if($_SESSION['user']['isAdmin']==0){
        $quizs = DB::query("SELECT * FROM quizs");
                
        return $view->render($response, 'studentsDetail_userview.html.twig', [
            'quizs' =>$quizs
        ]);
    }


    return $view->render($response, 'account.html.twig');
});


$app->get('/account/{id}/{userstatus}', function (Request $request, Response $response, array $args) {

    if (!isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }
    $view = Twig::fromRequest($request);

    if ($_SESSION['user']['id'] == $args['id']) {
        $id = $args['id'];

        switch ($args['userstatus']) {
            case "parent_info":
                $records = DB::query("SELECT * FROM users WHERE isAdmin!=1 ");
                return $view->render($response, 'parentDetail_teacherview.html.twig', [
                    'records' =>  $records
                ]);

            case "studnet_info":
                $records = DB::query("SELECT * FROM students ");
                return $view->render($response, 'studentsDetail_teacherview.html.twig', [
                    'records' =>  $records
                ]);

            case "teacher_info":
                $records = DB::query("SELECT * FROM users WHERE isAdmin=1 ");
                return $view->render($response, 'teacherDetail_teacherview.html.twig', [
                    'records' =>  $records
                ]);
            case "parent_view":
                $record = DB::queryFirstRow("SELECT * FROM users WHERE isAdmin=0 and id=$id ");
                $quizs = DB::query("SELECT * FROM quizs");
                $studentid=DB::queryFirstRow("SELECT id FROM students WHERE parentid=$id");
                
                return $view->render($response, 'parentDetail_userview.html.twig', [
                    'record' =>  $record,'quizs' =>$quizs,'studentid'=>$studentid
                ]);

            case "student_view":
                $records = DB::query("SELECT * FROM students where parentid=$id");
                $quizs = DB::query("SELECT * FROM quizs");               
                return $view->render($response, 'studentsDetail_userview.html.twig', [
                    'records' =>  $records,'quizs' =>$quizs
                ]);

            default:
                return $response->withHeader('Location', '/forbidden');
        }


    } else {
        return $response->withHeader('Location', '/forbidden');
    }
});


$app->post('/account/{id}/student_view', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();
    $username = $postvars['name'];
    $studentid = $postvars['studentid'];
    $parentid = $args['id'];
    $errorArray = "";
    $photoChange=1;


    if (!preg_match('/^[a-zA-Z][a-zA-Z 0-9]{5,19}$/', $username)) {
        $errorArray = "Username must be 6-20 characters long, begin with a letter and only "
            . "consist of uppercase/lowercase letters, digits, and underscores";
    }

    $photofile = ($_FILES["file$studentid"]);
    if ($photofile['size']!=0) {
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $photofile["name"];
        $filetype = $photofile["type"];
        $filesize = $photofile["size"];

        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if (!array_key_exists($ext, $allowed)) $errorArray[] = "Error: Please select a valid file format.";

        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if ($filesize > $maxsize) $errorArray[] = "Error: File size is larger than the allowed limit.";

        // Verify MYME type of the file
        if (in_array($filetype, $allowed)) {
            // Check whether file exists before uploading it
            if (file_exists("uploads/students" . $filename)) {
                echo $filename . " is already exists.";
            } else {
                move_uploaded_file($photofile["tmp_name"], "uploads/students/" . $filename);
            }
        } else {
            $errorArray[] = "Error: There was a problem uploading your file. Please try again.";
        }
    } else {
        $photoChange=0;
    }


    $user = DB::queryFirstRow("SELECT * FROM users  WHERE id=%s", $parentid);
    if (!$user) {
        array_push($errorArray, "can't find parent id");
    }


    if ($errorArray) { // array not empty -> errors present
        // STATE 2: Failed submission
        return $view->render($response, 'updat_not_success.html.twig', [
            'errorArray' =>  $errorArray
        ]);
    } else {
        // STATE 3: Successful submission
        if($photoChange){
            DB::query("UPDATE students SET name=%s , photo=%s  WHERE id=%i", $username, $filename, $studentid);
        }
        else{
            DB::query("UPDATE students SET name=%s  WHERE id=%i", $username, $studentid);
        }
        // $records = DB::query("SELECT * FROM students where parentid=$parentid");
        return $view->render($response, 'updat_success.html.twig');
    }
});

$app->post('/account/{id}/parent_view', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();
    $username = $postvars['name'];
    $address = $postvars['address'];
    $phone = $postvars['phone'];
    $parentid = $args['id'];
    $errorArray = "";



    if ($_SESSION['user']['id'] != $args['id']) {
        return $response->withHeader('Location', '/');
    }

    if (!preg_match('/^[a-zA-Z][a-zA-Z 0-9]{5,19}$/', $username)) {
        $errorArray = "Username must be 6-20 characters long, begin with a letter and only "
            . "consist of uppercase/lowercase letters, digits, and underscores";
    }



    if ($errorArray) { // array not empty -> errors present
        // STATE 2: Failed submission
        return $view->render($response, 'updat_not_success.html.twig', [
            'errorArray' =>  $errorArray
        ]);
    } else {
        // STATE 3: Successful submission
        DB::query("UPDATE users SET name=%s,address=%s, phone=%s  WHERE id=%s", $username, $address,$phone, $parentid);
        return $view->render($response, 'updat_success.html.twig');
    }
});


$app->post('/take_quiz', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    if (!isset($_SESSION['user']) ) {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }

    $view = Twig::fromRequest($request);

    $json = $request->getBody();
    $respon = json_decode($json, true); // true makes it return an associative array instead of an object
    $quizid=$respon['quizid'];
    $studentid =$respon['studentid'];
    $hastakeQuizagain = DB::queryFirstRow("SELECT * FROM answers where quizid=$quizid and studentid =$studentid");
    // if($hastakeQuizagain){
    //     $response = $response->withStatus(401); // record created
    // }
 

    $answerArray=$respon['answerArray'];
    $answerString=implode("|",$answerArray);
    $rightAnswer = DB::query("SELECT rightAnswer FROM questions_en where quizid=$quizid ");
    $total=count($rightAnswer);
    $result=0;
    for ($x = 0; $x < $total; $x++) {
        $aaa=$answerArray[$x];
        $bbb=$rightAnswer[$x]['rightAnswer'];
        if($aaa===$bbb){
            $result++;
        }
      }
    $resultPerc=($result/$total)*100;

    DB::insert('answers', [
        'quizid' => $quizid,
        'studentid'=> $studentid,
        'resultPerc' => $resultPerc,
        'answerArray'=>$answerString ]  );
    $id = DB::insertId();
    $response = $response->withStatus(201); // record created
    $response->getBody()->write("you have submit your quiz. Quiz id is $id ");
  
    return $response;
});




// api to get quiz
$app->get('/api/get_quiz', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $quizRecords = DB::queryFirstRow("SELECT * FROM quizs" );
    if (!$quizRecords) { // not found
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    }
    $json = json_encode($quizRecords, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

function moveUploadedFile($directory, UploadedFileInterface $uploadedFile)
{
    //getUploadedPath() { return $this->file; }
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8));
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . '/' . $filename);

    return $filename;
}

$app->get('/Create_quiz', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        return $response->withHeader('Location', '/');
    }

    $records = DB::query("SELECT * FROM questions_en ");

    return $view->render($response, 'create_quiz_shq.html.twig', [
        'records' =>  $records
    ]);
});

$app->post('/Create_quiz', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    // only for authenticated users
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        // VERSION 1:
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
        // VERSION 2:
        // $response = $response->withStatus(403);
        // return $view->render($response, 'forbidden.html.twig');
    }
    
    // extract submitted values
    $postVars = $request->getParsedBody();
    $description = $postVars['description'];


    // FIXME: sanitize body - 1) only allow certain HTML tags, 2) make sure it is valid html
    // WARNING: If you forget to sanitize the body bad things may happen such as JavaScript injection

    // check validity
    $errorsArray = array();
    //


    if ($description == null) {
        array_push($errorsArray, "Description is required.");
    }

    if (isset($_POST['formSubmit'])) {
        $aDoor = $_POST['formDoor'];
        if (empty($aDoor)) {
            array_push($errorsArray, "You didn't select any buildings.");
        }else{
            $question = implode(";", $aDoor);
        }
    }
    //

    if ($errorsArray) { // STATE 2: Failed submission

        return $view->render($response, 'create_quiz_shq.html.twig', [
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } else { // STATE 3: Success

        DB::insert('quizs', [
            'description' => $description,
            'questionIncluded' => $question
                    ]);

        return $view->render($response, 'create_questions_success.html.twig');
    }
});


$app->get('/display_result', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    
    if (!isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $parentid = $_SESSION['user']['id'];
    $studentArray=[];
    $studentlist = DB::query("SELECT * FROM students where  parentid=%s", $parentid);
  
    foreach ($studentlist as  $student) {
        $studentArray[]=$student['id'];
    }

    $quizs = DB::query("SELECT * FROM quizs");
                
    $answerlist =DB::query( "SELECT * FROM `answers` WHERE `studentid`  IN (".implode($studentArray).")");
  
    return $view->render($response, 'display_result.html.twig', [
        'studentlist' =>  $studentlist ,'parentid' => $parentid,'answerlist' =>$answerlist,'quizs' =>$quizs
    ]);

   
    
});





/***************Qin's codes end      ************************/
/****/
/***************Ziming's codes begin      ************************/



//*********** create question*/
$app->get('/Create_questions', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        return $response->withHeader('Location', '/');
    }
    return $view->render($response, 'create_questions.html.twig');
});

$app->post('/Create_questions', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    // only for authenticated users
    if (!isset($_SESSION['user'])) {
        // VERSION 1:
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
        // VERSION 2:
        // $response = $response->withStatus(403);
        // return $view->render($response, 'forbidden.html.twig');
    }
    // extract submitted values
    $postVars = $request->getParsedBody();
    $question = $postVars['question'];
    $a1 = $postVars['a1'];
    $a2 = $postVars['a2'];
    $a3 = $postVars['a3'];
    $a4 = $postVars['a4'];
    $rightAnswer = $postVars['rightAnswer'];
    $level = $postVars['level'];

    // FIXME: sanitize body - 1) only allow certain HTML tags, 2) make sure it is valid html
    // WARNING: If you forget to sanitize the body bad things may happen such as JavaScript injection

    // check validity
    $errorsArray = array();
    //

    // photo
    if (isset($_FILES['photo'])) {

        $fileSize = $_FILES['photo']['size'];
        $fileTmp = $_FILES['photo']['tmp_name'];
        if ($fileSize > 1024 * 1024 * 2) {
            array_push($errorsArray, "Photo file size too big. Must be 2MB or smaller.");
        } else {
            $imageInfo = getimagesize($fileTmp);
            if (!$imageInfo) {
                array_push($errorsArray, "Uploaded file is not a valid image (jpg, png, gif).");
            } else {
                /* echo "<pre>\n";
                print_r($imageInfo);
                echo "</pre>\n"; */
                $width = $imageInfo[0];
                $height = $imageInfo[1];
                $mimeType = $imageInfo['mime'];
                if ($width < 20 || $width > 1500 || $height < 20 || $height > 1500) {
                    array_push($errorsArray, "Image width and height must be in 200-1000 pixels range");
                } else {
                    $ext = "";
                    switch ($mimeType) {
                        case 'image/gif':
                            $ext = 'gif';
                            break;
                        case 'image/png':
                            $ext = 'png';
                            break;
                        case 'image/jpeg':
                            $ext = 'jpeg';
                            break;
                        default: // reject any other format
                            array_push($errorsArray, "Image must be gif, png or jpeg type");
                    }
                }
            }
        }
    } else {

        array_push($errorsArray, "Photo is required.");
    }

    $photoFilePath = "uploadimages/" . basename($_FILES["photo"]["name"]);

    if (file_exists($photoFilePath)) {
        array_push($errorsArray, "Photo is already exist.");
    } else {


        if (move_uploaded_file($fileTmp, $photoFilePath)) {
            //echo "The File ". $photoFilePath . " is upload.";

        } else {
            //echo "Error upload.";
        }
    }
    //
    if ($errorsArray) { // STATE 2: Failed submission
        return $view->render($response, 'create_questions.html.twig', [
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } else { // STATE 3: Success
        // $photoFilePath = "/" .  $photoFilePath;

        DB::insert('questions_en', [
            'question_description' => $question,
            'photo' => $photoFilePath,
            'a1' => $a1, 'a2' => $a2, 'a3' => $a3, 'a4' => $a4,
            'rightAnswer' => $rightAnswer, 'level' => $level
        ]);
        $newId = DB::insertId(); // get the id of last inserted record

        return $view->render($response, 'create_questions_success.html.twig');
    }
});
//*******end create question */
//*********** create quiz*/
$app->get('/Create_quizs', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        return $response->withHeader('Location', '/');
    }
    return $view->render($response, 'create_quizs.html.twig');
});

$app->post('/Create_quizs', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    // only for authenticated users
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        // VERSION 1:
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
        // VERSION 2:
        // $response = $response->withStatus(403);
        // return $view->render($response, 'forbidden.html.twig');
    }
    // extract submitted values
    $postVars = $request->getParsedBody();
    $description = $postVars['description'];
    $question = $postVars['question'];



    // FIXME: sanitize body - 1) only allow certain HTML tags, 2) make sure it is valid html
    // WARNING: If you forget to sanitize the body bad things may happen such as JavaScript injection

    // check validity
    $errorsArray = array();
    //


    if ($description == null) {
        array_push($errorsArray, "Description is required.");
    }

    //

    if ($errorsArray) { // STATE 2: Failed submission

        return $view->render($response, 'create_quizs.html.twig', [
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } else { // STATE 3: Success
        // $photoFilePath = "/" .  $photoFilePath;

        DB::insert('quizs', [
            'description' => $description,
            'questionIncluded' => $question
        ]);
        $newId = DB::insertId(); // get the id of last inserted record

        return $view->render($response, 'create_questions_success.html.twig');
    }
});
//*******end create quiz */
//*********** create quizquestion*/
$app->map(['GET', 'POST'],'/create_quizquestion', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        return $response->withHeader('Location', '/');
    }
    $quizs = DB::query("SELECT description FROM quizs order by description");

    $questions = DB::query("SELECT * FROM questions_en ");
    if (!$questions) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    }
    return $view->render($response, 'create_quizquestion.html.twig', [ 'questions' => $questions, 'quizs' => $quizs]);
    
});





//*******end create quizquestion */
//*********** add quesion in quiz */

$app->map(['GET', 'POST'],'/add_questionInQuiz', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    
    $json = $request->getBody();
    $todo = json_decode($json, true); // true makes it return an associative array instead of an object
    // validate todo fields, 400 in case of error
    if ( ($result = validateTodo($todo)) !== TRUE) {
        global $log;
        $log->debug("POST /todos failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    
    DB::insert('quizquestion', $todo);
    $id = DB::insertId();
    $response = $response->withStatus(201); // record created
    $response->getBody()->write(json_encode($id));
    return $response;
});
///*******end add quesion in quiz */
//* add answer */

$app->map(['GET', 'POST'],'/add_answer', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    
    $json = $request->getBody();
    $todo = json_decode($json, true); // true makes it return an associative array instead of an object
    // validate todo fields, 400 in case of error
    if ( ($result = validateTodo($todo)) !== TRUE) {
        global $log;
        $log->debug("POST /todos failed from " .  $_SERVER['REMOTE_ADDR'] . ": " . $result);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    
    //DB::insert('quizquestion', $todo);
    DB::insert('answers', $todo);
    $id = DB::insertId();
    $response = $response->withStatus(201); // record created
    $response->getBody()->write(json_encode($id));
    return $response;
});


$app->get('/take_quiz/{id}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    if (!isset($_SESSION['user']) ) {
        return $response->withHeader('Location', '/');
    }
    $userid=$_SESSION['user']['id'];
    $quizs = DB::query("SELECT * FROM quizs");
    $quizId=$args['id'];
    $quizInfo=DB::queryFirstRow("SELECT * FROM quizs where id=$quizId ");
    $questionsINquiz = DB::query("SELECT * FROM questions_en where quizid=$quizId ");
    $kids = DB::query("SELECT * FROM students where parentid=$userid");
 
    return $view->render($response, 'take_quizs_shq.html.twig', [
        'records' =>  $questionsINquiz,
        'quiz'=>$quizInfo,
        'userid'=>$userid,
        'quizs' =>$quizs,
        "kids"=>$kids            
    ]);
});






//*******end add answer */
//*********** create game*/
$app->get('/Create_games', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        return $response->withHeader('Location', '/');
    }
    return $view->render($response, 'create_games.html.twig');
});

$app->post('/Create_games', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    // only for authenticated users
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        // VERSION 1:
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
        // VERSION 2:
        // $response = $response->withStatus(403);
        // return $view->render($response, 'forbidden.html.twig');
    }
    // extract submitted values
    $postVars = $request->getParsedBody();
    $description = $postVars['description'];



    // FIXME: sanitize body - 1) only allow certain HTML tags, 2) make sure it is valid html
    // WARNING: If you forget to sanitize the body bad things may happen such as JavaScript injection

    // check validity
    $errorsArray = array();
    //


    if ($description == null) {
        array_push($errorsArray, "Description is required.");
    }

    //

    if ($errorsArray) { // STATE 2: Failed submission

        return $view->render($response, 'create_games.html.twig', [
            'v' => $postVars, 'errorsArray' => $errorsArray
        ]);
    } else { // STATE 3: Success
        // $photoFilePath = "/" .  $photoFilePath;

        DB::insert('games', ['description' => $description]);
        $newId = DB::insertId(); // get the id of last inserted record

        return $view->render($response, 'create_questions_success.html.twig');
    }
});
//*******end create game */
//*******display quiz */
$app->get('/display_quizs', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        return $response->withHeader('Location', '/');
    }
    $quizs = DB::query("SELECT description FROM quizs order by description");

    return $view->render($response, 'display_quiz.html.twig', [
        'records' =>  $quizs
    ]);
});
//*******end display quiz */
//* display quiz question */
$app->get('/display_quizQuestion', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    /*
    $userId = getAuthUserId($request->getHeaders());
    if (!$userId) {
        $response = $response->withStatus(403); // FIXME: should really be 401 instead, but not for JS
        $log->debug("GET /todos refused 403 - user/pass invalid");
        $response->getBody()->write(json_encode("403 - authentication failed"));
        return $response;
    }
    */
    //
    $paramArray = $request->getQueryParams();
    $sortBy = isset($paramArray['sortBy']) ? $paramArray['sortBy'] : "id";
    $quiz = isset($paramArray['quiz']) ? $paramArray['quiz'] : "Quiz1";
    
    /*
    if (!in_array($sortBy, ['id', 'task', 'dueDate', 'isDone'])) {
        global $log;
        $log->debug("GET /todos failed due to invalid sortBy from " .  $_SERVER['REMOTE_ADDR'] . ": " . $sortBy);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - invalid sortBy value"));
        return $response;
    }
    */
    $todoList = DB::query("SELECT distinct q1.questionId, q2.id,q2.question_description, 
	    q2.a1, q2.a2, q2.a3, q2.a4, q2.rightAnswer, q2.photo 
        FROM quizquestion q1 
        Join questions_en q2 on q2.id=q1.questionId
        WHERE q1.quiz=%s ORDER BY %l", $quiz, $sortBy);
    $json = json_encode($todoList, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});



//* end display quiz question */
//* display result */



//* end display result */
//* teacher display result */
$app->get('/teacher_display_result', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        return $response->withHeader('Location', '/');
    }

    
    $records = DB::query("SELECT * FROM quizs ");
    $users = DB::query("SELECT * FROM users WHERE isAdmin IS Null");
    return $view->render($response, 'teacher_display_result.html.twig', [
        'records' =>  $records ,'users' => $users
    ]);

   
    
});


//* end display result */
//* ajax display result */
$app->get('/ajax_display_result', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    /*
    $userId = getAuthUserId($request->getHeaders());
    if (!$userId) {
        $response = $response->withStatus(403); // FIXME: should really be 401 instead, but not for JS
        $log->debug("GET /todos refused 403 - user/pass invalid");
        $response->getBody()->write(json_encode("403 - authentication failed"));
        return $response;
    }
    */
    //
    $paramArray = $request->getQueryParams();
    $sortBy = isset($paramArray['sortBy']) ? $paramArray['sortBy'] : "id";
    $quiz = isset($paramArray['quiz']) ? $paramArray['quiz'] : "Quiz1";
    $userId = $_SESSION['user']['id'];
    /*
    if (!in_array($sortBy, ['id', 'task', 'dueDate', 'isDone'])) {
        global $log;
        $log->debug("GET /todos failed due to invalid sortBy from " .  $_SERVER['REMOTE_ADDR'] . ": " . $sortBy);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - invalid sortBy value"));
        return $response;
    }
    */
    $todoList = DB::query("SELECT distinct q2.photo , q2.question_description, 
    a.rightAnswer, a.answer
    FROM answers a 
    Join questions_en q2 on q2.id=a.questionId
    Join quizQuestion q1 on q1.questionId=q2.id
	WHERE a.quiz=%s AND a.studentid=%s AND a.id IN (
		SELECT MAX(id) FROM answers
		GROUP BY questionId)", $quiz, $userId);
    $json = json_encode($todoList, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});


//* end ajax display result */
//* ajax teacher display result
$app->get('/ajax_teacher_display_result', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    /*
    $userId = getAuthUserId($request->getHeaders());
    if (!$userId) {
        $response = $response->withStatus(403); // FIXME: should really be 401 instead, but not for JS
        $log->debug("GET /todos refused 403 - user/pass invalid");
        $response->getBody()->write(json_encode("403 - authentication failed"));
        return $response;
    }
    */
    //
    $paramArray = $request->getQueryParams();
    $sortBy = isset($paramArray['sortBy']) ? $paramArray['sortBy'] : "id";
    $quiz = isset($paramArray['quiz']) ? $paramArray['quiz'] : "ALL";
    $studentId = isset($paramArray['studentId']) ? $paramArray['studentId'] : "0";
    /*
    if (!in_array($sortBy, ['id', 'task', 'dueDate', 'isDone'])) {
        global $log;
        $log->debug("GET /todos failed due to invalid sortBy from " .  $_SERVER['REMOTE_ADDR'] . ": " . $sortBy);
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - invalid sortBy value"));
        return $response;
    }
    */
    if($quiz=='ALL' && $studentId == 0){
        $todoList = DB::query("SELECT distinct a.quiz, u.name, q2.photo , q2.question_description, 
    a.rightAnswer, a.answer
    FROM answers a 
    Join questions_en q2 on q2.id=a.questionId
    Join quizQuestion q1 on q1.questionId=q2.id
    Join users u on u.id=a.studentId
	WHERE  a.id IN (
		SELECT MAX(id) FROM answers
		GROUP BY studentId, quiz, questionId)");
    }
    if($quiz=='ALL' && $studentId > 0){
        $todoList = DB::query("SELECT distinct a.quiz, u.name, q2.photo , q2.question_description, 
    a.rightAnswer, a.answer
    FROM answers a 
    Join questions_en q2 on q2.id=a.questionId
    Join quizQuestion q1 on q1.questionId=q2.id
    Join users u on u.id=a.studentId
	WHERE  a.studentId=%s AND a.id IN (
		SELECT MAX(id) FROM answers
		GROUP BY studentId, quiz, questionId)",$studentId);
    }
    if($quiz !='ALL' && $studentId == 0){
        $todoList = DB::query("SELECT distinct a.quiz, u.name, q2.photo , q2.question_description, 
        a.rightAnswer, a.answer
        FROM answers a 
        Join questions_en q2 on q2.id=a.questionId
        Join quizQuestion q1 on q1.questionId=q2.id
        Join users u on u.id=a.studentId
	WHERE a.quiz=%s  AND  a.id IN (
		SELECT MAX(id) FROM answers
		GROUP BY studentId, quiz, questionId)", $quiz);
    }
    if($quiz !='ALL' && $studentId > 0){
        $todoList = DB::query("SELECT distinct a.quiz, u.name, q2.photo , q2.question_description, 
    a.rightAnswer, a.answer
    FROM answers a 
    Join questions_en q2 on q2.id=a.questionId
    Join quizQuestion q1 on q1.questionId=q2.id
    Join users u on u.id=a.studentId
	WHERE a.quiz=%s  AND a.studentId=%s AND a.id IN (
		SELECT MAX(id) FROM answers
		GROUP BY studentId, quiz, questionId)", $quiz, $studentId);
    }
    
    $json = json_encode($todoList, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});


//* end ajax teacher display result */
//*******display games */
$app->get('/display_games', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $records = DB::query("SELECT * FROM games ");
    return $view->render($response, 'display_games.html.twig', [
        'records' =>  $records
    ]);
});
//*******end display games */
//*******play games */
$app->get('/play_games', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $records = DB::query("SELECT * FROM games ");
    return $view->render($response, 'play_games.html.twig', [
        'records' =>  $records
    ]);
});
//*******end display games */
//*******display question */
$app->get('/display_questions', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user']) ||  $_SESSION['isAdmin'] != 1) {
        return $response->withHeader('Location', '/');
    }

    $records = DB::query("SELECT * FROM questions_en ");
    return $view->render($response, 'display_questions.html.twig', [
        'records' =>  $records
    ]);
});
//*******end display questions */
//*******take question */
$app->get('/take_questions', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user'])) {
        return $response->withHeader('Location', '/');
    }

    $records = DB::query("SELECT * FROM questions_en ");
    return $view->render($response, 'take_questions.html.twig', [
        'records' =>  $records
    ]);
});
//*******end take questions */
//* take quiz */



/*

$app->post('/take_quiz', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    if (!isset($_SESSION['user']) ) {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
    
 
    $json = $request->getBody();
    $respon = json_decode($json, true); // true makes it return an associative array instead of an object
    $quizid=$respon['quizid'];
    $studentid =$respon['studentid'];
    $answerArray=$respon['answerArray'];
    $answerString=implode("|",$answerArray);
    $rightAnswer = DB::query("SELECT rightAnswer FROM questions_en where quizid=$quizid ");
    $result=array_diff($answerArray,$rightAnswer);
    $total=count($rightAnswer);
    $gotWrong=count($result);
    $resultPerc=(1-$gotWrong/$total)*100;

    DB::insert('answers', [
        'quizid' => $quizid,
        'studentid'=> $studentid,
        'resultPerc' => $resultPerc,
        'answerArray'=>$answerString ]  );
    $id = DB::insertId();
    $response = $response->withStatus(201); // record created
    $response->getBody()->write("you have submit your quiz. Quiz id is $id ");
    $response->getBody()->write("$rightAnswer");
    return $response;
});
*/
//* end take quiz */
//*****google map */

$app->get('/googleMap', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    return $view->render($response, 'googleMap.html.twig');
});
//*****end google map */

/***************Ziming's codes end      ************************/

periodicCleanup();

// once every 100 pages are accessed attempt a cleanup
function periodicCleanup()
{
    if (rand(1, 1000) != 1) return;
    // remove all records from 'passwordresets' table that have expired
    // remove all unconfirmed accounts that were created more than X time ago
    // e-shop: remove cart items created 24 hours age or older
}


// return TRUE if all is fine otherwise return string describing the problem
function validateTodo($todo, $forPatch = false) {
    global $log;
    if ($todo === NULL) { // if json_decode fails it returns null - handle it here
        return "Invalid JSON data provided";
    }
    /*
    // does it have all the fields required and only the fields requried?
    $expectedFields = ['task', 'dueDate', 'isDone'];
    $todoFields = array_keys($todo);
    // check for fields that should not be there
    if (($diff = array_diff($todoFields, $expectedFields))) {
        return "Invalid fields in Todo: [" . implode(',',$diff) . "]";
    }
    if (!$forPatch) {
    // check for fields that are missing
        if (($diff = array_diff($expectedFields, $todoFields))) {
            return "Missing fields in Todo: [" . implode(',',$diff) . "]";
        }
    }
    // do not allow any field to be null (for both put and patch)
    foreach ($todo as $key => $value) {
        if (@is_null($value)) {
            return "$key must not be null";
        }
    }
    // validate each field
    if (isset($todo['task'])) { // in patch it may be absent
        if (is_null($todo['task'])) {
            return "Task description must not be null";
        }
        $task = $todo['task'];
        if (strlen($task) < 1 || strlen($task) > 100) {
            return "Task description must be 1-100 characters long";
        }
    }
    if (isset($todo['dueDate'])) {
        if (!date_create_from_format('Y-m-d', $todo['dueDate'])) {
            return "DueDate has invalid format";
        }
        $dueDate = strtotime($todo['dueDate']);
        if ($dueDate < strtotime('1900-01-01') || $dueDate > strtotime('2100-01-01')) {
            return "DueDate must be within 1900 to 2099 years";
        }
    }
    if (isset($todo['isDone'])) {
        if (!in_array($todo['isDone'], ['pending', 'done'])) {
            return "IsDone invalid: must be pending or done";
        }
    }
    //
    */
    return TRUE;
}





$app->run();
