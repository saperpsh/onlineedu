<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_f582cecf3ba03ccd274661cb60f7600b7965154f844f05f6933c85c3adb88919 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "\tRegister
";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "\t<link rel=\"stylesheet\" href=\"css/index.css\"/>

\t<script>
        \$(document).ready(function() {
\t\t\t \$('input[name=email]').on('keyup keypress blur change cut paste', function() {
                var email = \$('input[name=email]').val();
\t\t\t\tif(email)
\t\t\t\t\t\$('#isTaken').load(\"/checkemailavailable/\" + email);\t\t
            });
        });
        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>

";
    }

    // line 23
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "\t";
        if (($context["errorArray"] ?? null)) {
            // line 25
            echo "\t\t<ul>
\t\t\t";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 27
                echo "\t\t\t\t<li>";
                echo twig_escape_filter($this->env, $context["err"], "html", null, true);
                echo "</li>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "\t\t</ul>
\t";
        }
        // line 31
        echo "
\t<div class=\"d-flex justify-content-center form_container\">
\t\t<form method=\"post\">
\t\t\t<h2>Sign Up</h2>
\t\t\t<p>Please fill in this form to create an account!</p>
\t\t\t<hr>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"col-xs-6\"><input type=\"text\" class=\"form-control\"  name=\"username\" placeholder=\"username\" value=\"";
        // line 38
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "name", [], "any", false, false, false, 38), "html", null, true);
        echo "\" required=\"required\"></div>
\t
\t\t\t</div>

\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"Email\"  value=\"";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "email", [], "any", false, false, false, 43), "html", null, true);
        echo "\" required=\"required\">
\t\t\t\t<div class=\"col-xs-6\"><span id='isTaken'></span></div>
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"pass1\" placeholder=\"Password\" required=\"required\">
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"pass2\" placeholder=\"Confirm Password\" required=\"required\">
\t\t\t</div>

\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"submit\" class=\"btn btn-primary btn-lg\">Sign Up</button>
\t\t\t</div>
\t\t</form>
\t</div>

\t
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 43,  117 => 38,  108 => 31,  104 => 29,  95 => 27,  91 => 26,  88 => 25,  85 => 24,  81 => 23,  61 => 6,  57 => 5,  52 => 3,  48 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %}
\tRegister
{% endblock title %}
{% block head %}
\t<link rel=\"stylesheet\" href=\"css/index.css\"/>

\t<script>
        \$(document).ready(function() {
\t\t\t \$('input[name=email]').on('keyup keypress blur change cut paste', function() {
                var email = \$('input[name=email]').val();
\t\t\t\tif(email)
\t\t\t\t\t\$('#isTaken').load(\"/checkemailavailable/\" + email);\t\t
            });
        });
        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>

{% endblock %}
{% block content %}
\t{% if errorArray %}
\t\t<ul>
\t\t\t{% for err in errorArray %}
\t\t\t\t<li>{{ err }}</li>
\t\t\t{% endfor %}
\t\t</ul>
\t{% endif %}

\t<div class=\"d-flex justify-content-center form_container\">
\t\t<form method=\"post\">
\t\t\t<h2>Sign Up</h2>
\t\t\t<p>Please fill in this form to create an account!</p>
\t\t\t<hr>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"col-xs-6\"><input type=\"text\" class=\"form-control\"  name=\"username\" placeholder=\"username\" value=\"{{v.name}}\" required=\"required\"></div>
\t
\t\t\t</div>

\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"Email\"  value=\"{{v.email}}\" required=\"required\">
\t\t\t\t<div class=\"col-xs-6\"><span id='isTaken'></span></div>
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"pass1\" placeholder=\"Password\" required=\"required\">
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"pass2\" placeholder=\"Confirm Password\" required=\"required\">
\t\t\t</div>

\t\t
\t\t\t<div class=\"form-group\">
\t\t\t\t<button type=\"submit\" class=\"btn btn-primary btn-lg\">Sign Up</button>
\t\t\t</div>
\t\t</form>
\t</div>

\t
{% endblock content %}
", "register.html.twig", "C:\\xampp\\htdocs\\ipd20\\onlineEdu\\onlineeduproject\\templates\\register.html.twig");
    }
}
