<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register_isusernametaken.html.twig */
class __TwigTemplate_5fd76f5c8070237a4392609bddb387d7ecefabd496e55ac8e0daa8fbdea368e4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["isTaken"] ?? null)) {
            // line 2
            echo "    Username already in use. Choose a different one.
";
        }
    }

    public function getTemplateName()
    {
        return "register_isusernametaken.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if isTaken %}
    Username already in use. Choose a different one.
{% endif %}", "register_isusernametaken.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\register_isusernametaken.html.twig");
    }
}
