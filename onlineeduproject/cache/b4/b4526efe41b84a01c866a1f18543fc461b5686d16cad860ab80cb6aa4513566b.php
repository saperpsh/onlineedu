<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* sayhello.html.twig */
class __TwigTemplate_e5dea91ee690975ebab19207be3a9e5a732a894f62dac15d3263df3065ff83b2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "sayhello.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " say hello  ";
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
";
        // line 4
        if (($context["errorArray"] ?? null)) {
            // line 5
            echo "    <ul>
        ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 7
                echo "            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo "    </ul>
";
        }
        // line 11
        echo "
    <form clase method='POST' >
        <div class='form-group'>
            <label for=\"name\">Name: </lable><input type='text' id='name' name='name' value=\"";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "name", [], "any", false, false, false, 14), "html", null, true);
        echo "\">
            
        </div>
        <div class='form-group'>
            <label for=\"age\">age:   </lable><input type='text' id='age' name='age' value=\"";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "age", [], "any", false, false, false, 18), "html", null, true);
        echo "\">
        </div>

        <button type=\"submit\" class=\"btn btn-primary mb-2\">Say hello</button>
    </form>

";
    }

    public function getTemplateName()
    {
        return "sayhello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 18,  87 => 14,  82 => 11,  78 => 9,  69 => 7,  65 => 6,  62 => 5,  60 => 4,  54 => 3,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %} say hello  {% endblock title %}  
{% block content %} 
{% if errorArray %}
    <ul>
        {% for error in errorArray %}
            <li>{{error}}</li>
        {% endfor %}
    </ul>
{% endif %}

    <form clase method='POST' >
        <div class='form-group'>
            <label for=\"name\">Name: </lable><input type='text' id='name' name='name' value=\"{{v.name}}\">
            
        </div>
        <div class='form-group'>
            <label for=\"age\">age:   </lable><input type='text' id='age' name='age' value=\"{{v.age}}\">
        </div>

        <button type=\"submit\" class=\"btn btn-primary mb-2\">Say hello</button>
    </form>

{% endblock content %}  ", "sayhello.html.twig", "C:\\xampp\\htdocs\\ipd20\\day04slimfirst\\templates\\sayhello.html.twig");
    }
}
