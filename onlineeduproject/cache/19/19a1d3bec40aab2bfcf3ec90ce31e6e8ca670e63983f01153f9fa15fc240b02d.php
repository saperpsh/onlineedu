<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* article_view.html.twig */
class __TwigTemplate_e8f67397a6b2942139c963db3526ac02a0134d8f4ca0500b109b521f04c30257 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "article_view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " login  ";
    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
    ";
        // line 5
        if (($context["errorArray"] ?? null)) {
            // line 6
            echo "        <ul>
            ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 8
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["err"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "        </ul>
    ";
        }
        // line 12
        echo "
    
    
   ";
        // line 15
        if (($context["result"] ?? null)) {
            // line 16
            echo "        <h4 >title:  ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["result"] ?? null), "title", [], "any", false, false, false, 16), "html", null, true);
            echo " </h4>
            <div>  ";
            // line 17
            echo twig_get_attribute($this->env, $this->source, ($context["result"] ?? null), "body", [], "any", false, false, false, 17);
            echo " </div>
            <div> <a href=\"/\" > comment </a> </div>


    ";
        }
        // line 22
        echo "    
";
    }

    public function getTemplateName()
    {
        return "article_view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 22,  94 => 17,  89 => 16,  87 => 15,  82 => 12,  78 => 10,  69 => 8,  65 => 7,  62 => 6,  60 => 5,  54 => 4,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %} login  {% endblock title %}  

{% block content %} 
    {% if errorArray %}
        <ul>
            {% for err in errorArray %}
                <li>{{ err }}</li>
            {% endfor %}
        </ul>
    {% endif %}

    
    
   {% if result %}
        <h4 >title:  {{result.title}} </h4>
            <div>  {{result.body | raw}} </div>
            <div> <a href=\"/\" > comment </a> </div>


    {% endif %}
    
{% endblock content %}  ", "article_view.html.twig", "C:\\xampp\\htdocs\\ipd20\\day04slimblog\\templates\\article_view.html.twig");
    }
}
