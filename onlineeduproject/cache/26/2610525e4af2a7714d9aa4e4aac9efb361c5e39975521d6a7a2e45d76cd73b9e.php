<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* articleaddedit.html.twig */
class __TwigTemplate_bc2757a1a031a65c7a5ea882fa8a2e3096a3a7576d970d91c8ea6a05ade37be3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "articleaddedit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " login  ";
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "        <script src=\"https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js\"></script>
        <script>tinymce.init({selector:'textarea'});</script>
";
    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
    
     ";
        // line 11
        if (($context["errorArray"] ?? null)) {
            // line 12
            echo "        <ul>
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 14
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["err"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "        </ul>
    ";
        }
        // line 18
        echo "

    <p> this is  article add edit</p>
    <form method=\"post\">
        Title: <input type=\"text\" name=\"title\" value=";
        // line 22
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "><br>
        <textarea cols=60 rows=10 name=\"body\"  >";
        // line 23
        echo twig_escape_filter($this->env, ($context["body"] ?? null), "html", null, true);
        echo "</textarea><br>
        <input type=\"submit\" value=\"Add article\">
    </form>
    

";
    }

    public function getTemplateName()
    {
        return "articleaddedit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 23,  100 => 22,  94 => 18,  90 => 16,  81 => 14,  77 => 13,  74 => 12,  72 => 11,  65 => 9,  59 => 5,  55 => 4,  48 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %} login  {% endblock title %}  

{% block head %}
        <script src=\"https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js\"></script>
        <script>tinymce.init({selector:'textarea'});</script>
{% endblock %}

{% block content %} 
    
     {% if errorArray %}
        <ul>
            {% for err in errorArray %}
                <li>{{ err }}</li>
            {% endfor %}
        </ul>
    {% endif %}


    <p> this is  article add edit</p>
    <form method=\"post\">
        Title: <input type=\"text\" name=\"title\" value={{title}}><br>
        <textarea cols=60 rows=10 name=\"body\"  >{{body}}</textarea><br>
        <input type=\"submit\" value=\"Add article\">
    </form>
    

{% endblock content %}  ", "articleaddedit.html.twig", "C:\\xampp\\htdocs\\ipd20\\day04slimblog\\templates\\articleaddedit.html.twig");
    }
}
