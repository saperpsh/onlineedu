<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* article_success.html.twig */
class __TwigTemplate_187e91cf2db64093bac9a801526eefb5910346f8a4b3bd89e30faede6d28e572 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "article_success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " article success  ";
    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " 
    ";
        // line 5
        if (($context["errorArray"] ?? null)) {
            // line 6
            echo "        <ul>
            ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 8
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["err"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "        </ul>
    ";
        }
        // line 12
        echo "<p> Hello  ";
        echo twig_escape_filter($this->env, ($context["username"] ?? null), "html", null, true);
        echo "</p>
    <p>Article added successfully. <a href=\"/article/";
        // line 13
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\">Click here</a> to view new article.</p>

";
    }

    public function getTemplateName()
    {
        return "article_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 13,  82 => 12,  78 => 10,  69 => 8,  65 => 7,  62 => 6,  60 => 5,  54 => 4,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %} article success  {% endblock title %}  

{% block content %} 
    {% if errorArray %}
        <ul>
            {% for err in errorArray %}
                <li>{{ err }}</li>
            {% endfor %}
        </ul>
    {% endif %}
<p> Hello  {{username}}</p>
    <p>Article added successfully. <a href=\"/article/{{ id }}\">Click here</a> to view new article.</p>

{% endblock content %}  ", "article_success.html.twig", "C:\\xampp\\htdocs\\ipd20\\day04slimblog\\templates\\article_success.html.twig");
    }
}
