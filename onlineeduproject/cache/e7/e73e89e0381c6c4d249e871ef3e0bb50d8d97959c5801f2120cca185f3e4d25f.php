<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* category_view.html.twig */
class __TwigTemplate_1ecc79ae060ec4515b4e163434914aad600572ea1eb2e59824f8c7a3adbb040b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "category_view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Welcome
";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "\t<link rel=\"stylesheet\" href=\"http://day06eshop.ipd20:8888/css/dd.css\">
\t<script src=\"https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js\"></script>\t
";
    }

    // line 10
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "<!-- Page Content -->
\t<section class=\"py-5 text-center\">
\t\t<div class=\"container\">\t\t\t
\t\t\t<p class=\"text-muted mb-5 text-center\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 16
        if (($context["products"] ?? null)) {
            // line 17
            echo "\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["products"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 18
                echo "\t\t\t\t\t\t<div class=\"col-sm-6 col-lg-4 mb-3\">
\t\t\t\t\t\t\t<div> 
\t\t\t\t\t\t\t\t<h2><a href=\"/category/";
                // line 20
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "categoryId", [], "any", false, false, false, 20), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "name", [], "any", false, false, false, 20), "html", null, true);
                echo "</a></h2> \t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<h4> ";
                // line 21
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "description", [], "any", false, false, false, 21), "html", null, true);
                echo " </h4>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "\t\t\t\t";
        }
        // line 26
        echo "\t\t\t\t
\t\t\t\t
\t\t\t
\t\t\t</div>
\t\t</div>
\t</section>
\t<section class=\"py-5 text-center\">
\t\t";
        // line 33
        if (($context["totalpages"] ?? null)) {
            // line 34
            echo "\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, ($context["totalpages"] ?? null)));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 35
                echo "\t\t\t\t<a href=\"/category/";
                echo twig_escape_filter($this->env, ($context["categoryid"] ?? null), "html", null, true);
                echo "/page/";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["page"], "html", null, true);
                echo "</a>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "\t\t";
        }
        // line 38
        echo "\t</section>





";
    }

    public function getTemplateName()
    {
        return "category_view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 38,  136 => 37,  123 => 35,  118 => 34,  116 => 33,  107 => 26,  104 => 25,  94 => 21,  88 => 20,  84 => 18,  79 => 17,  77 => 16,  70 => 11,  66 => 10,  60 => 6,  56 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Welcome
{% endblock %}
{% block head %}
\t<link rel=\"stylesheet\" href=\"http://day06eshop.ipd20:8888/css/dd.css\">
\t<script src=\"https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js\"></script>\t
{% endblock %}

{% block content %}
<!-- Page Content -->
\t<section class=\"py-5 text-center\">
\t\t<div class=\"container\">\t\t\t
\t\t\t<p class=\"text-muted mb-5 text-center\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
\t\t\t<div class=\"row\">
\t\t\t\t{% if products %}
\t\t\t\t\t{% for product in products %}
\t\t\t\t\t\t<div class=\"col-sm-6 col-lg-4 mb-3\">
\t\t\t\t\t\t\t<div> 
\t\t\t\t\t\t\t\t<h2><a href=\"/category/{{product.categoryId}}\">{{ product.name }}</a></h2> \t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<h4> {{ product.description }} </h4>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t{% endfor %}
\t\t\t\t{% endif %}
\t\t\t\t
\t\t\t\t
\t\t\t
\t\t\t</div>
\t\t</div>
\t</section>
\t<section class=\"py-5 text-center\">
\t\t{% if totalpages %}
\t\t\t{% for page in  1..totalpages %}
\t\t\t\t<a href=\"/category/{{categoryid}}/page/{{page}}\">{{ page }}</a>
\t\t\t{% endfor %}
\t\t{% endif %}
\t</section>





{% endblock %}
", "category_view.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\category_view.html.twig");
    }
}
